package com.example.leroymerlin.data.product.list

import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity

interface ProductListLocalDataSource {
    suspend fun loadAllProduct(): List<ProductListEntity>
    suspend fun saveListDefaultValue(productList :List<ProductListEntity>)
}