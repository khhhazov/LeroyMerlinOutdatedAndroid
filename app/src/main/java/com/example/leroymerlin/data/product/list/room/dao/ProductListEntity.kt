package com.example.leroymerlin.data.product.list.room.dao

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity.Companion.TABLE_NAME
import java.io.Serializable
import java.sql.Blob

@Entity(tableName = TABLE_NAME)
data class ProductListEntity (
    @PrimaryKey
    @ColumnInfo (name = "productId")
    var id: Int,
    @ColumnInfo (name = "productName")
    var name: String,
    @ColumnInfo (name = "productImageUrl")
    var imageUrl: String
): Serializable {
    companion object {
        const val TABLE_NAME = "product_list_entities_table"
    }
}