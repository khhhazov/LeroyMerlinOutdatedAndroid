package com.example.leroymerlin.data.product.list.room.dao

import androidx.room.*

@Dao
interface ProductListDao {
    @Query("SELECT * FROM ${ProductListEntity.TABLE_NAME}")
    suspend fun loadAllProducts(): List<ProductListEntity>

    @Insert(entity = ProductListEntity::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun addProduct(productEntity: ProductListEntity)

    @Delete(entity = ProductListEntity::class)
    suspend fun deleteProduct(productEntity: ProductListEntity)
}