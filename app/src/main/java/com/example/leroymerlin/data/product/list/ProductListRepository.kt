package com.example.leroymerlin.data.product.list

import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity

class ProductListRepository(
    private val productListLocalDataSource: ProductListLocalDataSource
) {
    suspend fun fetchProductList(): List<ProductListEntity> {
        return productListLocalDataSource.loadAllProduct()
    }

    suspend fun saveDefaultValue(list: List<ProductListEntity>) {
        productListLocalDataSource.saveListDefaultValue(list)
    }
}