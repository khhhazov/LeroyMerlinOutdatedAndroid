package com.example.leroymerlin.data.product.list.room

import com.example.leroymerlin.data.product.list.ProductListLocalDataSource
import com.example.leroymerlin.data.product.list.room.dao.ProductListDao
import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity

class RoomProductListDataSource(private val productListDao: ProductListDao): ProductListLocalDataSource {
    override suspend fun loadAllProduct(): List<ProductListEntity> {
        return productListDao.loadAllProducts()
    }

//    override suspend fun fullCleanDataBase() {
//        val productList: List<ProductListEntity> = loadAllProduct()
//        for (item in productList) {
//            productListDao.deleteProduct(item)
//        }
//    }

    override suspend fun saveListDefaultValue(productList: List<ProductListEntity>) {
        for (item in productList) {
            productListDao.addProduct(item)
        }
    }

}