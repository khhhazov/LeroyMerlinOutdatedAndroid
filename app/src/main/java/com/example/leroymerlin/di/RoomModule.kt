package com.example.leroymerlin.di

import android.content.Context
import androidx.room.Room
import com.example.leroymerlin.data.base.ProductRoomDataBase
import com.example.leroymerlin.data.product.list.room.dao.ProductListDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RoomModule {

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context) =
        Room.databaseBuilder(
            context,
            ProductRoomDataBase::class.java,
            "product_room_database"
        ).build()
}