package com.example.leroymerlin.ui.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.example.leroymerlin.R
import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity

class CategoriesAdapter(context: Context, cardList: ArrayList<ProductListEntity>): RecyclerView.Adapter<CategoriesAdapter.CategoriesViewHolder>() {
    private var cardList: ArrayList<ProductListEntity>
    private var context: Context

    init {
        this.context = context
        this.cardList = cardList
    }

    @NonNull
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view: View = LayoutInflater.from(context)
            .inflate(R.layout.item_categories, parent, false)
        return CategoriesViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        val card: ProductListEntity = cardList[position]

        holder.textView.text = card.name

//        holder.imageView.setImageDrawable()
    }

    override fun getItemCount(): Int {
        return cardList.size
    }

    class CategoriesViewHolder(view: View): RecyclerView.ViewHolder(view) {
        var imageView: ImageView
        var textView: TextView
        init {
            imageView = view.findViewById(R.id.item_cardview__image)
            textView = view.findViewById(R.id.item_cardview__name)
        }
    }
}