package com.example.leroymerlin.ui.adapters

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.leroymerlin.R
import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity
import java.io.IOException


class ProductAdapter(context: Context, productList: ArrayList<ProductListEntity>): RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {
    private var productList: ArrayList<ProductListEntity>
    private var context: Context

    init {
        this.context = context
        this.productList = productList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view: View = LayoutInflater.from(context)
            .inflate(R.layout.item_product_list, parent, false)
        return ProductViewHolder(view, productList)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product: ProductListEntity = productList[position]

        holder.textView.text = product.name

        try {
            val ims = context.assets.open(product.imageUrl)
            val drw = Drawable.createFromStream(ims, null)
            holder.imageView.setImageDrawable(drw)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return productList.size
    }

    class ProductViewHolder(
        view: View,
        productList: ArrayList<ProductListEntity>
    ): RecyclerView.ViewHolder(view) {
        var imageView: ImageView
        var textView: TextView
        init {
            imageView = view.findViewById(R.id.item_product_list__image)
            textView = view.findViewById(R.id.product_info__name)
            view.setOnClickListener {
                val product: ProductListEntity = productList[layoutPosition]
                val bundle = Bundle()
                bundle.putSerializable("data", product)
                view.findNavController().navigate(R.id.productInfoFragment, bundle)
            }
        }
    }
}