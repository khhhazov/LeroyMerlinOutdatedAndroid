package com.example.leroymerlin.ui.main.product.info

import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.leroymerlin.databinding.FragmentProductInfoBinding

class ProductInfoFragment : Fragment() {
    private lateinit var binding: FragmentProductInfoBinding
    private lateinit var viewModel: ProductInfoViewModel
//    private lateinit var product: ModelProduct

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        product = arguments?.getSerializable("data") as ModelProduct
//        binding.productInfoName.text = product.productName
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProductInfoBinding.inflate(layoutInflater)
        val toolbar = binding.productInfoToolbar
        toolbar.overflowIcon?.setColorFilter(Color.BLACK , PorterDuff.Mode.SRC_ATOP);
        toolbar.setNavigationOnClickListener {
            activity?.onBackPressed()
        }
        return binding.root
    }
}