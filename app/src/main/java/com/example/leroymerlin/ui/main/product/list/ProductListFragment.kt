package com.example.leroymerlin.ui.main.product.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.leroymerlin.data.product.list.room.dao.ProductListEntity
import com.example.leroymerlin.databinding.FragmentProductListBinding
import com.example.leroymerlin.ui.adapters.CategoriesAdapter
import com.example.leroymerlin.ui.adapters.ProductAdapter

class ProductListFragment : Fragment() {
    private lateinit var binding: FragmentProductListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val product1 = ProductListEntity(1, "hello", "test")
        val product2 = ProductListEntity(2, "wow", "test")
        val product3 = ProductListEntity(3, "working", "test")
        val productList: ArrayList<ProductListEntity> = ArrayList()
        productList.add(product1)
        productList.add(product2)
        productList.add(product3)

        val categoriesAdapter = CategoriesAdapter(requireContext(), productList)
        val productAdapter = ProductAdapter(requireContext(), productList)

        binding = FragmentProductListBinding.inflate(layoutInflater)
        binding.productListRecycler.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        binding.productListRecycler.adapter = categoriesAdapter

        binding.productListMainRecycler.layoutManager =  LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.productListMainRecycler.adapter = productAdapter

        return binding.root
    }
}