package com.example.leroymerlin.ui.main.product.list

import androidx.lifecycle.ViewModel
import com.example.leroymerlin.data.product.list.ProductListRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProductListViewModel @Inject constructor(
    val productListRepository: ProductListRepository
) : ViewModel() {

}