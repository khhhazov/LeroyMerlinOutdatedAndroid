package com.example.leroymerlin.ui.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.leroymerlin.R
import com.example.leroymerlin.data.product.list.ProductListInitData
import com.example.leroymerlin.ui.main.product.list.ProductListViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var productListViewModel: ProductListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val productInitData = ProductListInitData()

        productListViewModel = ViewModelProvider(this)[ProductListViewModel::class.java]
        productListViewModel
    }
}